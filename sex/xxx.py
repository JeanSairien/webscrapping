from bs4 import BeautifulSoup
import requests, bs4, sys, urllib.parse


search =  input ("Quelle est votre recherche? ")

print(search)

# Make a request
title = requests.get(
    "https://www.iafd.com/results.asp?searchtype=comprehensive&searchstring="+ search)

#define an array
all_td = []
# parsing using xml
soup = BeautifulSoup(title.content, 'html')
# find all td
current = soup.find_all('td')
# loop over current
for td in current:
    item = (td.get_text()) # catch the text
    all_td.append({"item:", item}) # put in the array


print(all_td) # print all result of request



# <class 'bs4.element.NavigableString'>
