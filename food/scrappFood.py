import requests, bs4, sys, urllib.parse

search = urllib.parse.quote(' '.join(sys.argv[1:])).replace('%20','+')
res = requests.get('https://duckduckgo.com/html?q='+search, {'User-Agent': 'DuckDuckGogler'})
soup = bs4.BeautifulSoup(res.text, 'html.parser')
if "If this error persists, please let us know: ops@duckduckgo.com" in soup:
    print("Sorry, sometimes duckduckgo doesn't want to work properly. Try running the script again.")
    sys.exit()

results = soup.select('.web-result')
rnum = 0
for i in results:
    rnum += 1
    info = []
    for item in i.text.splitlines():
        if item.startswith("                  "):
            item = item.replace("                  ","")
        if not item == '' and not item == ' ':
            info.append(item)
    link = i.a['href'].replace("/l/?kh=-1&uddg=","")
    print('\033[96m'+str(rnum)+': \033[92m'+info[0]+'\033[0m')
    print('\033[93m'+urllib.parse.unquote(link)+'\033[0m')
    print(info[1]+'\n\n')

    # https://duckduckgo.com/?q=restaurant+reunion+974&t=ffab&ia=places
