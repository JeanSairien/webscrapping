import requests
from bs4 import BeautifulSoup

headers = {
    "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:84.0) Gecko/20100101 Firefox/84.0",
}

engine = "https://duckduckgo.com/html/?q=restaurent+reunion"


page = requests.get(engine, headers=headers).text
soup = BeautifulSoup(page, 'html.parser').find_all("a", class_="result__url", href=True)


for link in soup:
    print(link['href'])
    f = open("restorator.txt", "a")
    f.write("\n"+link['href'])
    f.close()

#open and read the file after the appending:
f = open("restorator.txt", "r")
print(f.read())



guide = "https://guide-reunion.fr/tourisme-loisirs/sortir/restaurant/"
resto = requests.get(guide, headers=headers).text
soup = BeautifulSoup(resto, 'html.parser').find_all("a")

for item in soup:
    print(item['href'])
    f = open("restorator2.txt", "a")
    f.write("\n"+"https://guide-reunion.fr"+item['href'])
    f.close()

#open and read the file after the appending:
f = open("restorator2.txt", "r")
print(f.read())

# https://guide-reunion.fr/tourisme-loisirs/sortir/restaurant/
