
UBISOFT +

# définire les réels données a a éxtraire:

_nom - date de parution - prix (sauf si abonnement)_

**Apres éxamins du store ubisoft et ubisoft+ il apparait que les jeux se retrouve d'une partie a l'autre**

**La procédure suite a la problématique énoncée est simple:**
**Etant donner que les données relative a la parution d'un jeux semble être le plus gros problème a extraire**
**voici la marche a suivre:**

**Pour ma part j'ai utiliser plusieurs librairie notament curl et un module de symphonie: DomCrawler**

**Grace a _Curl_ j'effectue une requete qui va aller questionner le site , cette requette a pour but de recuperer l'integralité du code html disponible a l'adresse indiquer**

**Comme la page contient un nombre limité de jeux j'execute une fonction javascript qui simule le scroll jusqu'a la fin de la page pour récuperer la totalité des jeux et ainsi leur nom**

**Tout d'abord j'ai cibler la div qui contient le nom des jeux, grace a _DomCrawler_ j'extrait tout les nom disponible sur la page et les assignes a une variable**

**Le probleme etant de récuperer les références des jeux et en particulier la date de parution _(release_date)_ j'ai donc décider d'étudier la structure html et j'ai donc pu déterminer que les jeux afficher dans le catalogue du site et sur ubi+ sont les memes et au meme tarifs avec les meme informations seule le prix des jeux peux changer en fonction de l'abonnement et des avantages tarifaires a être abonner**

## Plusieurs solutions s'offre a vous :

* la plus simple mais lourde:
**Il vous suffit d'extraire le nom des jeux puis d'effectuer une seconde requette pour questionner la barre de recherche _(https://store.ubi.com/ie/search?q=)_ avec le nom du jeux comme variable, vous obtiendrai donc la page associer avec toute les informations nécessaire il ne vous restera qu'a cibler les balises qui contienne les informations recherchées**

* plus complexe mais moins lourde:
**Dans la page ubisoft+ se trouve un lien caché pour chaque jeux il vous suffirait pour chaques jeux d'extraire ce lien et d'effectuer un nouvelle requette dessus afin de récuperer vos informations**
