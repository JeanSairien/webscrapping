<?php


// require
// php-xml
// php-curl
// php-zip

//composer:
// symfony/dom-crawler
// symfony/css-selector
// php-webdriver/webdriver
// symfony/browser-kit
// symfony/http-client


//scroll:
// javascript: window.scrollTo(0,document.body.scrollHeight);

//target:
// https://store.ubi.com/ie/ubisoftplus/games

require 'vendor/autoload.php';

use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\BrowserKit\HttpBrowser;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\BrowserKit\AbstractBrowser;
use Symfony\Component\BrowserKit\Response;



$browser = new HttpBrowser(HttpClient::create());

$browser->xmlHttpRequest('GET', 'https://store.ubi.com/ie/ubisoftplus/games');
// $browser->clickLink('Load More');
$main = $browser->filter('div.sample')->text();

var_dump($main);
