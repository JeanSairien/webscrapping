<?php

require 'vendor/autoload.php';

use GuzzleHttp\Client;

// Create a client with a base URI
$client = new GuzzleHttp\Client(['base_uri' => 'https://store.ubi.com/ie/ubisoftplus/games']);
// Send a request to https://foo.com/api/test
$response = $client->request('GET', 'https://store.ubi.com/ie/ubisoftplus/games');
// Send a request to https://foo.com/root
// $response = $client->request('GET', '/root');

var_dump($response);


?>
