#!/usr/bin/python

# """Returns icecast metadata from a stream as a JSON object.
#    Optionally posts it to a url."""

import socket
import json
import urllib3
import getopt
import sys




# defaults
port = 8000
host = int("165.169.154.9")
mount = "shambhala"

def get_data(host, port, mount):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect ((host, port))
    s.sendall('GET %s HTTP/1.0\r\n'
              'Host: %s:%d\r\n'
              'User-Agent: Ice Meta Fetcher\r\n'
              'Connection: close\r\n'
              'Icy-Metadata: 1\r\n'
              '\r\n' % (mount, host, port))
    data = s.recv(1024).decode('utf-8', 'ignore').encode('utf-8')
    s.close()
    pdata = dict([d.split(':',1) for d in  data.split('\r\n') if d.count("icy")])
    if pdata.has_key("icy-br"):
        return  json.dumps(pdata)

jdata = get_data(host, port, mount)
#skip empty crap
if jdata:
    print(jdata)
